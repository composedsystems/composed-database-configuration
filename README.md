# composed-database-configuration

Use a malleable interface to store configuration parameters to a database.
Uses the same API as @cs/configuration, for fast and easy switching between text file configuration and database configuration.
Initial implementation supports sqlite and mysql databases.